#-------------------------------------------------
#
# Project created by QtCreator 2012-07-17T17:21:10
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = stv090x-iqconst
TEMPLATE = app

SOURCES += mainwindow.cpp \
    stv090x-iqconst.cpp
SOURCES += main.cpp

HEADERS  += mainwindow.h \
    stv090x-iqconst.h

FORMS    += mainwindow.ui

INCLUDEPATH += /usr/local/qwt-6.1.3-svn/include
INCLUDEPATH += /usr/include/qwt
LIBS += -Wl,-rpath,/usr/local/qwt-6.1.3-svn/lib -L /usr/local/qwt-6.1.3-svn/lib -l:libqwt.so

