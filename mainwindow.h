#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QVector>
#include <qwt_plot.h>
#include <qwt_plot_curve.h>
#include <qwt_symbol.h>
#include <qwt_plot_scaleitem.h>
#include <qwt_scale_engine.h>
#include "stv090x-iqconst.h"

namespace Ui {
    class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

public slots:
    void on_updateButton_clicked();
    void qwt_draw(QVector<double> x, QVector<double> y);

private slots:
    void on_actionExit_triggered();

private:
    Ui::MainWindow *ui;
    STV090Xiq myiq;
	QwtPlotCurve *curve_1, *curve_2, *curve_3;
	QwtPlotScaleItem *scaleX;
	QwtPlotScaleItem *scaleY;
	QwtSymbol *scatter_symbol_1, *scatter_symbol_2, *scatter_symbol_3;
    int s1,s2,s3;
    int ss1;
    int ss1color,ss2color,ss3color;
    int graph_scale;
};

#endif // MAINWINDOW_H
