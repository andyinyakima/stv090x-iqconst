/****************************************************************************
** Meta object code from reading C++ file 'stv090x-iqconst.h'
**
** Created: Fri Dec 6 10:57:48 2013
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.4)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "stv090x-iqconst.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'stv090x-iqconst.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.4. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_STV090Xiq[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       1,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: signature, parameters, type, tag, flags
      15,   11,   10,   10, 0x05,

       0        // eod
};

static const char qt_meta_stringdata_STV090Xiq[] = {
    "STV090Xiq\0\0x,y\0"
    "signaldraw(QVector<double>,QVector<double>)\0"
};

void STV090Xiq::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        STV090Xiq *_t = static_cast<STV090Xiq *>(_o);
        switch (_id) {
        case 0: _t->signaldraw((*reinterpret_cast< QVector<double>(*)>(_a[1])),(*reinterpret_cast< QVector<double>(*)>(_a[2]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData STV090Xiq::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject STV090Xiq::staticMetaObject = {
    { &QThread::staticMetaObject, qt_meta_stringdata_STV090Xiq,
      qt_meta_data_STV090Xiq, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &STV090Xiq::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *STV090Xiq::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *STV090Xiq::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_STV090Xiq))
        return static_cast<void*>(const_cast< STV090Xiq*>(this));
    return QThread::qt_metacast(_clname);
}

int STV090Xiq::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QThread::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 1)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 1;
    }
    return _id;
}

// SIGNAL 0
void STV090Xiq::signaldraw(QVector<double> _t1, QVector<double> _t2)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}
QT_END_MOC_NAMESPACE
