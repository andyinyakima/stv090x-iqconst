/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created: Fri Dec 6 10:57:46 2013
**      by: Qt User Interface Compiler version 4.8.4
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QAction>
#include <QApplication>
#include <QButtonGroup>
#include <QCheckBox>
#include <QComboBox>
#include <QDoubleSpinBox>
#include <QGridLayout>
#include <QGroupBox>
#include <QHeaderView>
#include <QLabel>
#include <QMainWindow>
#include <QMenu>
#include <QMenuBar>
#include <QPushButton>
#include <QRadioButton>
#include <QScrollBar>
#include <QSpinBox>
#include <QStatusBar>
#include <QWidget>
#include "qwt_plot.h"

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *actionExit;
    QWidget *centralWidget;
    QwtPlot *qwtPlot;
    QLabel *label_2;
    QWidget *gridLayoutWidget;
    QGridLayout *gridLayout;
    QLabel *label_3;
    QComboBox *adapterBox;
    QCheckBox *loopBox;
    QPushButton *updateButton;
    QLabel *label_5;
    QScrollBar *persistenceScrollBar;
    QGroupBox *const_sel_group_box;
    QRadioButton *const_sel_norm;
    QRadioButton *mode_radioButton_10;
    QRadioButton *mode_radioButton_20;
    QRadioButton *mode_radioButton_30;
    QRadioButton *mode_radioButton_40;
    QRadioButton *mode_radioButton_70;
    QRadioButton *mode_radioButton_50;
    QRadioButton *mode_radioButton_60;
    QRadioButton *mode_radioButton_80;
    QRadioButton *mode_radioButton_a0;
    QRadioButton *mode_radioButton_e0;
    QRadioButton *mode_radioButton_90;
    QRadioButton *mode_radioButton_d0;
    QRadioButton *mode_radioButton_c0;
    QRadioButton *mode_radioButton_b0;
    QRadioButton *mode_radioButton_f0;
    QGroupBox *IQ_pick_groupBox;
    QRadioButton *DemodOut_radioButton;
    QRadioButton *equalizer_radioButton;
    QRadioButton *deRot2_radioButton;
    QRadioButton *sym_inter_radioButton;
    QRadioButton *symbol_radioButton;
    QRadioButton *inter_radioButton;
    QRadioButton *deRot1_radioButton;
    QRadioButton *mismatch_radioButton;
    QRadioButton *DemodIn_radioButton;
    QRadioButton *symbol_radioButton_09;
    QRadioButton *symbol_radioButton_0a;
    QRadioButton *symbol_radioButton_0b;
    QRadioButton *symbol_radioButton_0c;
    QRadioButton *symbol_radioButton_0d;
    QRadioButton *symbol_radioButton_0e;
    QRadioButton *symbol_radioButton_0f;
    QLabel *label;
    QSpinBox *samples_spinBox;
    QLabel *label_4;
    QGroupBox *groupBox;
    QSpinBox *ss1_spinBox;
    QSpinBox *ss2_spinBox;
    QLabel *label_6;
    QLabel *label_7;
    QSpinBox *ss3_spinBox;
    QLabel *label_10;
    QDoubleSpinBox *mult_doubleSpinBox;
    QLabel *label_9;
    QGroupBox *Color_groupBox;
    QRadioButton *ColorBYR_radioButton;
    QRadioButton *ColorGray_radioButton;
    QSpinBox *spinBox_graphscale;
    QLabel *label_8;
    QStatusBar *statusBar;
    QMenuBar *menuBar;
    QMenu *menuFile;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(1033, 796);
        actionExit = new QAction(MainWindow);
        actionExit->setObjectName(QString::fromUtf8("actionExit"));
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        qwtPlot = new QwtPlot(centralWidget);
        qwtPlot->setObjectName(QString::fromUtf8("qwtPlot"));
        qwtPlot->setGeometry(QRect(200, 10, 620, 621));
        label_2 = new QLabel(centralWidget);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setGeometry(QRect(410, 730, 231, 20));
        QFont font;
        font.setPointSize(12);
        font.setBold(true);
        font.setWeight(75);
        label_2->setFont(font);
        gridLayoutWidget = new QWidget(centralWidget);
        gridLayoutWidget->setObjectName(QString::fromUtf8("gridLayoutWidget"));
        gridLayoutWidget->setGeometry(QRect(10, 10, 171, 104));
        gridLayout = new QGridLayout(gridLayoutWidget);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        gridLayout->setContentsMargins(0, 0, 0, 0);
        label_3 = new QLabel(gridLayoutWidget);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_3, 1, 0, 1, 1);

        adapterBox = new QComboBox(gridLayoutWidget);
        adapterBox->setObjectName(QString::fromUtf8("adapterBox"));

        gridLayout->addWidget(adapterBox, 0, 1, 1, 1);

        loopBox = new QCheckBox(gridLayoutWidget);
        loopBox->setObjectName(QString::fromUtf8("loopBox"));
        loopBox->setLayoutDirection(Qt::RightToLeft);
        loopBox->setChecked(true);

        gridLayout->addWidget(loopBox, 2, 0, 1, 1);

        updateButton = new QPushButton(gridLayoutWidget);
        updateButton->setObjectName(QString::fromUtf8("updateButton"));

        gridLayout->addWidget(updateButton, 2, 1, 1, 1);

        label_5 = new QLabel(gridLayoutWidget);
        label_5->setObjectName(QString::fromUtf8("label_5"));
        label_5->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_5, 0, 0, 1, 1);

        persistenceScrollBar = new QScrollBar(gridLayoutWidget);
        persistenceScrollBar->setObjectName(QString::fromUtf8("persistenceScrollBar"));
        persistenceScrollBar->setMinimum(5);
        persistenceScrollBar->setMaximum(25);
        persistenceScrollBar->setValue(15);
        persistenceScrollBar->setOrientation(Qt::Horizontal);

        gridLayout->addWidget(persistenceScrollBar, 1, 1, 1, 1);

        const_sel_group_box = new QGroupBox(centralWidget);
        const_sel_group_box->setObjectName(QString::fromUtf8("const_sel_group_box"));
        const_sel_group_box->setGeometry(QRect(10, 340, 181, 351));
        const_sel_norm = new QRadioButton(const_sel_group_box);
        const_sel_norm->setObjectName(QString::fromUtf8("const_sel_norm"));
        const_sel_norm->setGeometry(QRect(10, 20, 100, 21));
        const_sel_norm->setChecked(true);
        mode_radioButton_10 = new QRadioButton(const_sel_group_box);
        mode_radioButton_10->setObjectName(QString::fromUtf8("mode_radioButton_10"));
        mode_radioButton_10->setEnabled(true);
        mode_radioButton_10->setGeometry(QRect(10, 40, 131, 21));
        mode_radioButton_20 = new QRadioButton(const_sel_group_box);
        mode_radioButton_20->setObjectName(QString::fromUtf8("mode_radioButton_20"));
        mode_radioButton_20->setGeometry(QRect(10, 60, 131, 21));
        mode_radioButton_30 = new QRadioButton(const_sel_group_box);
        mode_radioButton_30->setObjectName(QString::fromUtf8("mode_radioButton_30"));
        mode_radioButton_30->setGeometry(QRect(10, 80, 131, 21));
        mode_radioButton_40 = new QRadioButton(const_sel_group_box);
        mode_radioButton_40->setObjectName(QString::fromUtf8("mode_radioButton_40"));
        mode_radioButton_40->setGeometry(QRect(10, 100, 131, 21));
        mode_radioButton_70 = new QRadioButton(const_sel_group_box);
        mode_radioButton_70->setObjectName(QString::fromUtf8("mode_radioButton_70"));
        mode_radioButton_70->setGeometry(QRect(10, 160, 131, 21));
        mode_radioButton_50 = new QRadioButton(const_sel_group_box);
        mode_radioButton_50->setObjectName(QString::fromUtf8("mode_radioButton_50"));
        mode_radioButton_50->setEnabled(true);
        mode_radioButton_50->setGeometry(QRect(10, 120, 131, 21));
        mode_radioButton_60 = new QRadioButton(const_sel_group_box);
        mode_radioButton_60->setObjectName(QString::fromUtf8("mode_radioButton_60"));
        mode_radioButton_60->setGeometry(QRect(10, 140, 131, 21));
        mode_radioButton_80 = new QRadioButton(const_sel_group_box);
        mode_radioButton_80->setObjectName(QString::fromUtf8("mode_radioButton_80"));
        mode_radioButton_80->setGeometry(QRect(10, 180, 131, 21));
        mode_radioButton_a0 = new QRadioButton(const_sel_group_box);
        mode_radioButton_a0->setObjectName(QString::fromUtf8("mode_radioButton_a0"));
        mode_radioButton_a0->setGeometry(QRect(10, 220, 131, 21));
        mode_radioButton_e0 = new QRadioButton(const_sel_group_box);
        mode_radioButton_e0->setObjectName(QString::fromUtf8("mode_radioButton_e0"));
        mode_radioButton_e0->setGeometry(QRect(10, 300, 131, 21));
        mode_radioButton_90 = new QRadioButton(const_sel_group_box);
        mode_radioButton_90->setObjectName(QString::fromUtf8("mode_radioButton_90"));
        mode_radioButton_90->setGeometry(QRect(10, 200, 131, 21));
        mode_radioButton_d0 = new QRadioButton(const_sel_group_box);
        mode_radioButton_d0->setObjectName(QString::fromUtf8("mode_radioButton_d0"));
        mode_radioButton_d0->setGeometry(QRect(10, 280, 131, 21));
        mode_radioButton_c0 = new QRadioButton(const_sel_group_box);
        mode_radioButton_c0->setObjectName(QString::fromUtf8("mode_radioButton_c0"));
        mode_radioButton_c0->setEnabled(true);
        mode_radioButton_c0->setGeometry(QRect(10, 260, 131, 21));
        mode_radioButton_b0 = new QRadioButton(const_sel_group_box);
        mode_radioButton_b0->setObjectName(QString::fromUtf8("mode_radioButton_b0"));
        mode_radioButton_b0->setGeometry(QRect(10, 240, 131, 21));
        mode_radioButton_f0 = new QRadioButton(const_sel_group_box);
        mode_radioButton_f0->setObjectName(QString::fromUtf8("mode_radioButton_f0"));
        mode_radioButton_f0->setGeometry(QRect(10, 320, 131, 21));
        IQ_pick_groupBox = new QGroupBox(centralWidget);
        IQ_pick_groupBox->setObjectName(QString::fromUtf8("IQ_pick_groupBox"));
        IQ_pick_groupBox->setGeometry(QRect(830, 330, 181, 351));
        DemodOut_radioButton = new QRadioButton(IQ_pick_groupBox);
        DemodOut_radioButton->setObjectName(QString::fromUtf8("DemodOut_radioButton"));
        DemodOut_radioButton->setGeometry(QRect(0, 20, 100, 21));
        DemodOut_radioButton->setChecked(true);
        equalizer_radioButton = new QRadioButton(IQ_pick_groupBox);
        equalizer_radioButton->setObjectName(QString::fromUtf8("equalizer_radioButton"));
        equalizer_radioButton->setGeometry(QRect(0, 40, 121, 21));
        deRot2_radioButton = new QRadioButton(IQ_pick_groupBox);
        deRot2_radioButton->setObjectName(QString::fromUtf8("deRot2_radioButton"));
        deRot2_radioButton->setGeometry(QRect(0, 60, 121, 21));
        sym_inter_radioButton = new QRadioButton(IQ_pick_groupBox);
        sym_inter_radioButton->setObjectName(QString::fromUtf8("sym_inter_radioButton"));
        sym_inter_radioButton->setGeometry(QRect(0, 80, 121, 21));
        symbol_radioButton = new QRadioButton(IQ_pick_groupBox);
        symbol_radioButton->setObjectName(QString::fromUtf8("symbol_radioButton"));
        symbol_radioButton->setGeometry(QRect(0, 100, 121, 21));
        inter_radioButton = new QRadioButton(IQ_pick_groupBox);
        inter_radioButton->setObjectName(QString::fromUtf8("inter_radioButton"));
        inter_radioButton->setGeometry(QRect(0, 120, 121, 21));
        deRot1_radioButton = new QRadioButton(IQ_pick_groupBox);
        deRot1_radioButton->setObjectName(QString::fromUtf8("deRot1_radioButton"));
        deRot1_radioButton->setGeometry(QRect(0, 140, 121, 21));
        mismatch_radioButton = new QRadioButton(IQ_pick_groupBox);
        mismatch_radioButton->setObjectName(QString::fromUtf8("mismatch_radioButton"));
        mismatch_radioButton->setGeometry(QRect(0, 160, 121, 21));
        DemodIn_radioButton = new QRadioButton(IQ_pick_groupBox);
        DemodIn_radioButton->setObjectName(QString::fromUtf8("DemodIn_radioButton"));
        DemodIn_radioButton->setGeometry(QRect(0, 180, 121, 21));
        symbol_radioButton_09 = new QRadioButton(IQ_pick_groupBox);
        symbol_radioButton_09->setObjectName(QString::fromUtf8("symbol_radioButton_09"));
        symbol_radioButton_09->setGeometry(QRect(0, 200, 121, 21));
        symbol_radioButton_0a = new QRadioButton(IQ_pick_groupBox);
        symbol_radioButton_0a->setObjectName(QString::fromUtf8("symbol_radioButton_0a"));
        symbol_radioButton_0a->setGeometry(QRect(0, 220, 121, 21));
        symbol_radioButton_0b = new QRadioButton(IQ_pick_groupBox);
        symbol_radioButton_0b->setObjectName(QString::fromUtf8("symbol_radioButton_0b"));
        symbol_radioButton_0b->setGeometry(QRect(0, 240, 121, 21));
        symbol_radioButton_0c = new QRadioButton(IQ_pick_groupBox);
        symbol_radioButton_0c->setObjectName(QString::fromUtf8("symbol_radioButton_0c"));
        symbol_radioButton_0c->setGeometry(QRect(0, 260, 121, 21));
        symbol_radioButton_0d = new QRadioButton(IQ_pick_groupBox);
        symbol_radioButton_0d->setObjectName(QString::fromUtf8("symbol_radioButton_0d"));
        symbol_radioButton_0d->setGeometry(QRect(0, 280, 121, 21));
        symbol_radioButton_0e = new QRadioButton(IQ_pick_groupBox);
        symbol_radioButton_0e->setObjectName(QString::fromUtf8("symbol_radioButton_0e"));
        symbol_radioButton_0e->setGeometry(QRect(0, 300, 121, 21));
        symbol_radioButton_0f = new QRadioButton(IQ_pick_groupBox);
        symbol_radioButton_0f->setObjectName(QString::fromUtf8("symbol_radioButton_0f"));
        symbol_radioButton_0f->setGeometry(QRect(0, 320, 121, 21));
        label = new QLabel(centralWidget);
        label->setObjectName(QString::fromUtf8("label"));
        label->setEnabled(true);
        label->setGeometry(QRect(50, 310, 141, 20));
        label->setFont(font);
        samples_spinBox = new QSpinBox(centralWidget);
        samples_spinBox->setObjectName(QString::fromUtf8("samples_spinBox"));
        samples_spinBox->setGeometry(QRect(840, 20, 47, 24));
        samples_spinBox->setMinimum(1);
        samples_spinBox->setMaximum(3);
        samples_spinBox->setSingleStep(1);
        samples_spinBox->setValue(1);
        label_4 = new QLabel(centralWidget);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        label_4->setGeometry(QRect(890, 20, 101, 20));
        groupBox = new QGroupBox(centralWidget);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        groupBox->setGeometry(QRect(830, 110, 191, 121));
        ss1_spinBox = new QSpinBox(groupBox);
        ss1_spinBox->setObjectName(QString::fromUtf8("ss1_spinBox"));
        ss1_spinBox->setGeometry(QRect(20, 30, 47, 24));
        ss1_spinBox->setMinimum(1);
        ss1_spinBox->setMaximum(10);
        ss1_spinBox->setValue(1);
        ss2_spinBox = new QSpinBox(groupBox);
        ss2_spinBox->setObjectName(QString::fromUtf8("ss2_spinBox"));
        ss2_spinBox->setGeometry(QRect(20, 60, 47, 24));
        ss2_spinBox->setMinimum(1);
        ss2_spinBox->setMaximum(10);
        ss2_spinBox->setValue(4);
        label_6 = new QLabel(groupBox);
        label_6->setObjectName(QString::fromUtf8("label_6"));
        label_6->setGeometry(QRect(70, 30, 111, 16));
        label_7 = new QLabel(groupBox);
        label_7->setObjectName(QString::fromUtf8("label_7"));
        label_7->setGeometry(QRect(70, 60, 111, 16));
        ss3_spinBox = new QSpinBox(groupBox);
        ss3_spinBox->setObjectName(QString::fromUtf8("ss3_spinBox"));
        ss3_spinBox->setGeometry(QRect(20, 90, 47, 24));
        ss3_spinBox->setMinimum(1);
        ss3_spinBox->setMaximum(10);
        ss3_spinBox->setValue(4);
        label_10 = new QLabel(groupBox);
        label_10->setObjectName(QString::fromUtf8("label_10"));
        label_10->setGeometry(QRect(70, 90, 111, 16));
        mult_doubleSpinBox = new QDoubleSpinBox(centralWidget);
        mult_doubleSpinBox->setObjectName(QString::fromUtf8("mult_doubleSpinBox"));
        mult_doubleSpinBox->setGeometry(QRect(840, 50, 66, 24));
        mult_doubleSpinBox->setDecimals(0);
        mult_doubleSpinBox->setMaximum(2048);
        mult_doubleSpinBox->setValue(256);
        label_9 = new QLabel(centralWidget);
        label_9->setObjectName(QString::fromUtf8("label_9"));
        label_9->setGeometry(QRect(910, 50, 101, 16));
        Color_groupBox = new QGroupBox(centralWidget);
        Color_groupBox->setObjectName(QString::fromUtf8("Color_groupBox"));
        Color_groupBox->setGeometry(QRect(830, 240, 191, 81));
        ColorBYR_radioButton = new QRadioButton(Color_groupBox);
        ColorBYR_radioButton->setObjectName(QString::fromUtf8("ColorBYR_radioButton"));
        ColorBYR_radioButton->setGeometry(QRect(10, 30, 121, 21));
        ColorBYR_radioButton->setChecked(true);
        ColorGray_radioButton = new QRadioButton(Color_groupBox);
        ColorGray_radioButton->setObjectName(QString::fromUtf8("ColorGray_radioButton"));
        ColorGray_radioButton->setGeometry(QRect(10, 50, 121, 21));
        spinBox_graphscale = new QSpinBox(centralWidget);
        spinBox_graphscale->setObjectName(QString::fromUtf8("spinBox_graphscale"));
        spinBox_graphscale->setGeometry(QRect(840, 80, 61, 24));
        spinBox_graphscale->setMinimum(75);
        spinBox_graphscale->setMaximum(130);
        spinBox_graphscale->setSingleStep(5);
        spinBox_graphscale->setValue(100);
        label_8 = new QLabel(centralWidget);
        label_8->setObjectName(QString::fromUtf8("label_8"));
        label_8->setGeometry(QRect(910, 80, 101, 16));
        MainWindow->setCentralWidget(centralWidget);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QString::fromUtf8("statusBar"));
        MainWindow->setStatusBar(statusBar);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QString::fromUtf8("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 1033, 20));
        menuFile = new QMenu(menuBar);
        menuFile->setObjectName(QString::fromUtf8("menuFile"));
        MainWindow->setMenuBar(menuBar);

        menuBar->addAction(menuFile->menuAction());
        menuFile->addAction(actionExit);

        retranslateUi(MainWindow);
        QObject::connect(updateButton, SIGNAL(clicked()), MainWindow, SLOT(on_updateButton_clicked()));

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "STV090X-iqconst", 0));
        actionExit->setText(QApplication::translate("MainWindow", "Exit", 0));
        label_2->setText(QApplication::translate("MainWindow", "Imaginary  (Quadrature)", 0));
        label_3->setText(QApplication::translate("MainWindow", "Persistence", 0));
        adapterBox->clear();
        adapterBox->insertItems(0, QStringList()
         << QApplication::translate("MainWindow", "0", 0)
         << QApplication::translate("MainWindow", "1", 0)
         << QApplication::translate("MainWindow", "2", 0)
         << QApplication::translate("MainWindow", "3", 0)
         << QApplication::translate("MainWindow", "4", 0)
         << QApplication::translate("MainWindow", "5", 0)
         << QApplication::translate("MainWindow", "6", 0)
         << QApplication::translate("MainWindow", "7", 0)
         << QApplication::translate("MainWindow", "8", 0)
        );
        loopBox->setText(QApplication::translate("MainWindow", "Loop", 0));
        updateButton->setText(QApplication::translate("MainWindow", "Update", 0));
        label_5->setText(QApplication::translate("MainWindow", "Adapter", 0));
        const_sel_group_box->setTitle(QApplication::translate("MainWindow", "High Nibble (IQCONST)", 0));
        const_sel_norm->setText(QApplication::translate("MainWindow", "Normal", 0));
        mode_radioButton_10->setText(QApplication::translate("MainWindow", "Reserved 0x10", 0));
        mode_radioButton_20->setText(QApplication::translate("MainWindow", "Reserved 0x20", 0));
        mode_radioButton_30->setText(QApplication::translate("MainWindow", "Reserved 0x30", 0));
        mode_radioButton_40->setText(QApplication::translate("MainWindow", "Reserved 0x40", 0));
        mode_radioButton_70->setText(QApplication::translate("MainWindow", "Reserved 0x70", 0));
        mode_radioButton_50->setText(QApplication::translate("MainWindow", "Reserved 0x50", 0));
        mode_radioButton_60->setText(QApplication::translate("MainWindow", "Reserved 0x60", 0));
        mode_radioButton_80->setText(QApplication::translate("MainWindow", "Reserved 0x80", 0));
        mode_radioButton_a0->setText(QApplication::translate("MainWindow", "Reserved 0xa0", 0));
        mode_radioButton_e0->setText(QApplication::translate("MainWindow", "Reserved 0xe0", 0));
        mode_radioButton_90->setText(QApplication::translate("MainWindow", "Reserved 0x90", 0));
        mode_radioButton_d0->setText(QApplication::translate("MainWindow", "Reserved 0xd0", 0));
        mode_radioButton_c0->setText(QApplication::translate("MainWindow", "Reserved 0xc0", 0));
        mode_radioButton_b0->setText(QApplication::translate("MainWindow", "Reserved 0xb0", 0));
        mode_radioButton_f0->setText(QApplication::translate("MainWindow", "Reserved 0xf0", 0));
        IQ_pick_groupBox->setTitle(QApplication::translate("MainWindow", "Low Nibble (IQCONST)", 0));
        DemodOut_radioButton->setText(QApplication::translate("MainWindow", " Normal", 0));
        equalizer_radioButton->setText(QApplication::translate("MainWindow", "Reserved 0x01", 0));
        deRot2_radioButton->setText(QApplication::translate("MainWindow", "Reserved 0x02", 0));
        sym_inter_radioButton->setText(QApplication::translate("MainWindow", "Reserved 0x03", 0));
        symbol_radioButton->setText(QApplication::translate("MainWindow", "Reserved 0x04", 0));
        inter_radioButton->setText(QApplication::translate("MainWindow", "Reserved 0x05", 0));
        deRot1_radioButton->setText(QApplication::translate("MainWindow", "Reserved 0x06", 0));
        mismatch_radioButton->setText(QApplication::translate("MainWindow", "Reserved 0x07", 0));
        DemodIn_radioButton->setText(QApplication::translate("MainWindow", "Reserved 0x08", 0));
        symbol_radioButton_09->setText(QApplication::translate("MainWindow", "Reserved 0x09", 0));
        symbol_radioButton_0a->setText(QApplication::translate("MainWindow", "Reserved 0x0a", 0));
        symbol_radioButton_0b->setText(QApplication::translate("MainWindow", "Reserved 0x0b", 0));
        symbol_radioButton_0c->setText(QApplication::translate("MainWindow", "Reserved 0x0c", 0));
        symbol_radioButton_0d->setText(QApplication::translate("MainWindow", "Reserved 0x0d", 0));
        symbol_radioButton_0e->setText(QApplication::translate("MainWindow", "Reserved 0x0e", 0));
        symbol_radioButton_0f->setText(QApplication::translate("MainWindow", "Reserved 0x0f", 0));
        label->setText(QApplication::translate("MainWindow", "Real (In Phase)", 0));
        label_4->setText(QApplication::translate("MainWindow", " x 256 Samples", 0));
        groupBox->setTitle(QApplication::translate("MainWindow", "Scatter Symbols", 0));
        label_6->setText(QApplication::translate("MainWindow", "Scat Sym_1 Size", 0));
        label_7->setText(QApplication::translate("MainWindow", "Scat Sym_2 Size", 0));
        label_10->setText(QApplication::translate("MainWindow", "Scat Sym_3 Size", 0));
        label_9->setText(QApplication::translate("MainWindow", "Multiplier", 0));
        Color_groupBox->setTitle(QApplication::translate("MainWindow", "Scat Sym Colors", 0));
        ColorBYR_radioButton->setText(QApplication::translate("MainWindow", "Blue,Yellow,Red", 0));
        ColorGray_radioButton->setText(QApplication::translate("MainWindow", "Grays", 0));
        label_8->setText(QApplication::translate("MainWindow", "Graph Scaler", 0));
        menuFile->setTitle(QApplication::translate("MainWindow", "File", 0));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
