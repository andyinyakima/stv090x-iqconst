#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    qRegisterMetaType<double>("double");
    qRegisterMetaType<QVector<double> >("QVector<double>");

    connect(&myiq, SIGNAL(signaldraw(QVector<double>, QVector<double>)), this, SLOT(qwt_draw(QVector<double>, QVector<double>)));

    curve_1 = new QwtPlotCurve("Curve 1");
    curve_1->setStyle(QwtPlotCurve::NoCurve);
	curve_1->attach(ui->qwtPlot);
	curve_2 = new QwtPlotCurve("Curve 2");
	curve_2->setStyle(QwtPlotCurve::NoCurve);
	curve_2->attach(ui->qwtPlot);
	curve_3 = new QwtPlotCurve("Curve 3");
    curve_3->setStyle(QwtPlotCurve::NoCurve);
	curve_3->attach(ui->qwtPlot);

    scatter_symbol_1 = new QwtSymbol;
    scatter_symbol_1->setStyle(QwtSymbol::Ellipse);
    scatter_symbol_1->setSize(s1,s1);  //was 5,5 mine 2,2
    scatter_symbol_1->setPen(QColor(ss1color));
    scatter_symbol_1->setBrush(QColor(ss1color));
    curve_1->setSymbol(scatter_symbol_1);
	scatter_symbol_2 = new QwtSymbol;
	scatter_symbol_2->setStyle(QwtSymbol::Ellipse);
    scatter_symbol_2->setSize(s2,s2);  //was 4,4 mine 10,10
    scatter_symbol_2->setPen(QColor(ss2color));   //was 200,0,0
    scatter_symbol_2->setBrush(QColor(ss2color));
    curve_2->setSymbol(scatter_symbol_2);
	scatter_symbol_3 = new QwtSymbol;
	scatter_symbol_3->setStyle(QwtSymbol::Ellipse);
    scatter_symbol_3->setSize(s3,s3); //was 5,5 mine 2,2
    scatter_symbol_3->setPen(QColor(ss3color)); // was 150,0,0
    scatter_symbol_3->setBrush(QColor(ss3color));
    curve_3->setSymbol(scatter_symbol_3);

    graph_scale =ui->spinBox_graphscale->value();
	ui->qwtPlot->setAxisTitle(QwtPlot::xBottom, "Real");
	ui->qwtPlot->setAxisTitle(QwtPlot::yLeft, "Imaginary");
    ui->qwtPlot->setAxisScale(QwtPlot::xBottom, -graph_scale, graph_scale);
    ui->qwtPlot->setAxisScale(QwtPlot::yLeft, -graph_scale,graph_scale);
	ui->qwtPlot->enableAxis(QwtPlot::xBottom ,0);
	ui->qwtPlot->enableAxis(QwtPlot::yLeft ,0);

	scaleX = new QwtPlotScaleItem();
	scaleX->setAlignment(QwtScaleDraw::BottomScale);
    scaleX->setScaleDiv((new QwtLinearScaleEngine())->divideScale(-graph_scale, graph_scale, 10, 5));
	scaleX->attach(ui->qwtPlot);
	scaleY = new QwtPlotScaleItem();
	scaleY->setAlignment(QwtScaleDraw::LeftScale);
    scaleY->setScaleDiv((new QwtLinearScaleEngine())->divideScale(-graph_scale, graph_scale, 10, 5));
	scaleY->attach(ui->qwtPlot);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::qwt_draw(QVector<double> x, QVector<double> y)
{
    double mplier = ui->mult_doubleSpinBox->value();  // originally 256
	QVector<double> x_1;
	QVector<double> y_1;
	QVector<double> xy_1;	
	QVector<double> x_2;
	QVector<double> y_2;
	QVector<double> xy_2;
	QVector<double> x_3;
	QVector<double> y_3;
	QVector<double> xy_3;

	cout << "qwt_draw()" << endl;
	
	for (int i = 0; i < x.size(); i++) {
        if (!xy_1.contains(x[i]*mplier + y[i])) {
            x_1.append((x[i]));
            y_1.append((y[i]));
            xy_1.append(x[i]*mplier + y[i]);
			continue;
		}
        if (!xy_2.contains(x[i]*mplier + y[i])) {
			x_2.append(x[i]);
			y_2.append(y[i]);
            xy_2.append(x[i]*mplier + y[i]);
			continue;
		}
        if (!xy_3.contains(x[i]*mplier + y[i])) {
			x_3.append(x[i]);
			y_3.append(y[i]);
            xy_3.append(x[i]*mplier + y[i]);
			continue;
		}
	}
	
	curve_1->setSamples(x_1, y_1);
	curve_2->setSamples(x_2, y_2);
    curve_3->setSamples(x_3, y_3);
    ui->qwtPlot->replot();

	myiq.setup(ui->adapterBox->currentText().toInt(), ui->loopBox->isChecked(), ui->persistenceScrollBar->value());
}

void MainWindow::on_updateButton_clicked()
{
    cout << "on_pushButton_clicked()" << endl;

    myiq.setup(ui->adapterBox->currentText().toInt(), ui->loopBox->isChecked(), ui->persistenceScrollBar->value());



    myiq.samp_and_mode=(ui->samples_spinBox->value())*256;

  // this is quick and messy below just to get things going
    if(ui->const_sel_norm->isChecked()==true)
        myiq.mode_select=0x00;
    if(ui->mode_radioButton_10->isChecked()==true)
        myiq.mode_select=0x10;
    if(ui->mode_radioButton_20->isChecked()==true)
        myiq.mode_select=0x20;
    if(ui->mode_radioButton_30->isChecked()==true)
        myiq.mode_select=0x30;
    if(ui->mode_radioButton_40->isChecked()==true)
        myiq.mode_select=0x40;
    if(ui->mode_radioButton_50->isChecked()==true)
        myiq.mode_select=0x50;
    if(ui->mode_radioButton_60->isChecked()==true)
        myiq.mode_select=0x60;
    if(ui->mode_radioButton_70->isChecked()==true)
        myiq.mode_select=0x70;
    if(ui->mode_radioButton_80->isChecked()==true)
        myiq.mode_select=0x80;
    if(ui->mode_radioButton_90->isChecked()==true)
        myiq.mode_select=0x90;
    if(ui->mode_radioButton_a0->isChecked()==true)
        myiq.mode_select=0xa0;
    if(ui->mode_radioButton_b0->isChecked()==true)
        myiq.mode_select=0xb0;
    if(ui->mode_radioButton_c0->isChecked()==true)
        myiq.mode_select=0xc0;
    if(ui->mode_radioButton_d0->isChecked()==true)
        myiq.mode_select=0xd0;
    if(ui->mode_radioButton_e0->isChecked()==true)
        myiq.mode_select=0xe0;
    if(ui->mode_radioButton_f0->isChecked()==true)
        myiq.mode_select=0xf0;


// this is quick and messy below just to get things going
    if(ui->DemodOut_radioButton->isChecked()==true)
        myiq.iq_pick_point=0x00;
    if(ui->equalizer_radioButton->isChecked()==true)
        myiq.iq_pick_point=0x01;
    if(ui->deRot2_radioButton->isChecked()==true)
        myiq.iq_pick_point=0x02;
    if(ui->sym_inter_radioButton->isChecked()==true)
        myiq.iq_pick_point=0x03;
    if(ui->symbol_radioButton->isChecked()==true)
        myiq.iq_pick_point=0x04;
    if(ui->inter_radioButton->isChecked()==true)
        myiq.iq_pick_point=0x05;
    if(ui->deRot1_radioButton->isChecked()==true)
        myiq.iq_pick_point=0x06;
    if(ui->mismatch_radioButton->isChecked()==true)
        myiq.iq_pick_point=0x07;
    if(ui->DemodIn_radioButton->isChecked()==true)
        myiq.iq_pick_point=0x08;
    if(ui->symbol_radioButton_09->isChecked()==true)
        myiq.iq_pick_point=0x09;
    if(ui->symbol_radioButton_0a->isChecked()==true)
        myiq.iq_pick_point=0x0a;
    if(ui->symbol_radioButton_0b->isChecked()==true)
        myiq.iq_pick_point=0x0b;
    if(ui->symbol_radioButton_0c->isChecked()==true)
        myiq.iq_pick_point=0x0c;
    if(ui->symbol_radioButton_0d->isChecked()==true)
        myiq.iq_pick_point=0x0d;
    if(ui->symbol_radioButton_0e->isChecked()==true)
        myiq.iq_pick_point=0x0e;
    if(ui->symbol_radioButton_0f->isChecked()==true)
        myiq.iq_pick_point=0x0f;

    s1 = ui->ss1_spinBox->value();
    scatter_symbol_1->setSize(s1,s1);
    s2 = ui->ss2_spinBox->value();
    scatter_symbol_2->setSize(s2,s2);
    s3 = ui->ss3_spinBox->value();
    scatter_symbol_3->setSize(s3,s3);
    if(ui->ColorBYR_radioButton->isChecked()==true)
    {
        ss1color = 0x0000ff;
        ss2color = 0xffff00;
        ss3color = 0xff0000;
    }
    if(ui->ColorGray_radioButton->isChecked()==true)
    {
        ss1color = 0x000000;
        ss2color = 0xffffff;
        ss3color = 0xaaaaaa;
    }
 /*   if(ui->ColorAlt_radioButton->isChecked()==true)
    {
        ss1color = 0x0000ff;
        ss2color = 0x00ff00;
        ss3color = 0xff0000;
    }*/
    scatter_symbol_1->setPen(QColor(ss1color));
    scatter_symbol_1->setBrush(QColor(ss1color));
    scatter_symbol_2->setPen(QColor(ss2color));   //was 200,0,0
    scatter_symbol_2->setBrush(QColor(ss2color));
    scatter_symbol_3->setPen(QColor(ss3color)); // was 150,0,0
    scatter_symbol_3->setBrush(QColor(ss3color));

    graph_scale =ui->spinBox_graphscale->value();
    ui->qwtPlot->setAxisScale(QwtPlot::xBottom, -graph_scale, graph_scale);
    ui->qwtPlot->setAxisScale(QwtPlot::yLeft, -graph_scale,graph_scale);
    scaleX->setScaleDiv((new QwtLinearScaleEngine())->divideScale(-graph_scale, graph_scale, 10, 5));
    scaleY->setScaleDiv((new QwtLinearScaleEngine())->divideScale(-graph_scale, graph_scale, 10, 5));

    myiq.start();

}

void MainWindow::on_actionExit_triggered()
{
    myiq.closeadapter();
    myiq.quit();
    myiq.wait();
    exit(1);
}
